FROM openjdk:8-jre-alpine
VOLUME /mnt
ADD /target/*.jar /mnt/restore-weather-service-latest.jar
COPY /target/*.jar /mnt/
RUN sh -c 'touch /mnt/restore-weather-service-latest.jar'
EXPOSE 9098:9098
ENTRYPOINT [ "sh", "-c", "java -Xmx1024m -jar /mnt/restore-weather-service-latest.jar" ]
