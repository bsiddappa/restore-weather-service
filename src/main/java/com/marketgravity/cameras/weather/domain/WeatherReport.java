package com.marketgravity.cameras.weather.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Data
@Entity
@Table(name = "weather_data")
public class WeatherReport implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int reportId;

    @Column(name="location_id")
    private int locationId;
    private String longitude;
    private String latitude;
    private int weather_id;
    private String weather_main;
    private String weather_description;
    private String weather_icon;
    private String base;
    private String main_temp;
    private String main_feels_like;
    private String main_temp_max;
    private String main_temp_min;
    private int main_pressure;
    private int main_humidity;
    private String wind_speed;
    private String wind_deg;
    private int clouds_all;
    private long dt;
    private int sys_type;
    private String sys_id;
    private String sys_country;
    private String sys_sunrise;
    private String sys_sunset;
    private String timezone;
    private String location_name;
    private String cod;


    public WeatherReport() {

    }

    public WeatherReport(int locationId, String longitude, String latitude, int weather_id, String weather_main, String weather_description, String weather_icon, String base, String main_temp, String main_feels_like, String main_temp_max, String main_temp_min, int main_pressure, int main_humidity, String wind_speed, String wind_deg, int clouds_all, long dt, int sys_type, String sys_id, String sys_country, String sys_sunrise, String sys_sunset, String timezone, String location_name, String cod) {
        this.locationId = locationId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.weather_id = weather_id;
        this.weather_main = weather_main;
        this.weather_description = weather_description;
        this.weather_icon = weather_icon;
        this.base = base;
        this.main_temp = main_temp;
        this.main_feels_like = main_feels_like;
        this.main_temp_max = main_temp_max;
        this.main_temp_min = main_temp_min;
        this.main_pressure = main_pressure;
        this.main_humidity = main_humidity;
        this.wind_speed = wind_speed;
        this.wind_deg = wind_deg;
        this.clouds_all = clouds_all;
        this.dt = dt;
        this.sys_type = sys_type;
        this.sys_id = sys_id;
        this.sys_country = sys_country;
        this.sys_sunrise = sys_sunrise;
        this.sys_sunset = sys_sunset;
        this.timezone = timezone;
        this.location_name = location_name;
        this.cod = cod;
    }
}


