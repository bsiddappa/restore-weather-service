package com.marketgravity.cameras.weather.service;

import com.marketgravity.cameras.weather.domain.WeatherReport;
import com.marketgravity.cameras.weather.dto.frontend.response.WeatherHistoricalDataBreakdownDto;
import com.marketgravity.cameras.weather.dto.frontend.response.WeatherHistoricalResponseDto;
import com.marketgravity.cameras.weather.repository.WeatherReportRepository;
import com.marketgravity.cameras.weather.util.DatesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    WeatherReportRepository weatherReportRepository;

    @Value("${timeZone}")
    private String timeZone;


    public WeatherHistoricalResponseDto weatherHistoricalByLocationId(int locationId,
                                                                        String timeStart,
                                                                        String timeEnd) {

        WeatherHistoricalResponseDto weatherHistoricalResponseDto = new WeatherHistoricalResponseDto();
        List<WeatherHistoricalDataBreakdownDto> weatherBreakDownList = new ArrayList<>();
        List<WeatherReport> weatherList= weatherReportRepository.findByDtBetweenAndLocationId(DatesUtil.timeConversion(timeStart), DatesUtil.timeConversion(timeEnd), locationId);
        for(WeatherReport rep : weatherList){
            WeatherHistoricalDataBreakdownDto dto = new WeatherHistoricalDataBreakdownDto();
            dto.setDate(DatesUtil.convertUnixDateToStrDate(rep.getDt()));
            dto.setIconCode(rep.getWeather_icon());
            dto.setMainTemp(rep.getMain_temp());
            dto.setMaxTemp(rep.getMain_temp_max());
            dto.setMinTemp(rep.getMain_temp_min());
            weatherBreakDownList.add(dto);
        }
        weatherHistoricalResponseDto.setLocationId(locationId);
        weatherHistoricalResponseDto.setWeatherHistoricalDataBreakdown(weatherBreakDownList);
        return weatherHistoricalResponseDto;
    }
}
