package com.marketgravity.cameras.weather.dto.frontend.response;

import lombok.Data;
import java.util.List;

@Data
public class WeatherHistoricalResponseDto {

    private int locationId;
    List<WeatherHistoricalDataBreakdownDto> weatherHistoricalDataBreakdown;

}
