package com.marketgravity.cameras.weather.dto.frontend.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraphqlRequest {

    private String query;
    private Map<String, Object> variables = new HashMap<>();
    private String operationName;

}
