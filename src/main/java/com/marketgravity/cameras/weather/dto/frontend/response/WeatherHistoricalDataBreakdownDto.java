package com.marketgravity.cameras.weather.dto.frontend.response;

import lombok.Data;

import java.util.Map;

@Data
public class WeatherHistoricalDataBreakdownDto {

    private String iconCode;
    private String mainTemp;
    private String maxTemp;
    private String minTemp;
    private String date;
}
