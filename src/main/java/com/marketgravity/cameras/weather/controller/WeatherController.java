package com.marketgravity.cameras.weather.controller;

import com.marketgravity.cameras.weather.dto.frontend.response.WeatherHistoricalResponseDto;
import com.marketgravity.cameras.weather.service.WeatherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WeatherController {

    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Autowired
    WeatherService weatherService;

    @GetMapping(value="/weather/location/{locationId}/{timeStart}/{timeEnd}")
    public WeatherHistoricalResponseDto weatherHistoricalByLocationId(@PathVariable("locationId") int locationId,
                                                                 @PathVariable("timeStart") String timeStart,
                                                                 @PathVariable("timeEnd") String timeEnd){

        logger.info("Query weather for locationId: " + locationId + " startTime: " +timeStart + " endTime: " +timeEnd);
        return weatherService.weatherHistoricalByLocationId(locationId, timeStart, timeEnd);
    }

}
