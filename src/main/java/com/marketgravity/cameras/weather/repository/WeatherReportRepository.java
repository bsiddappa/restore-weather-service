package com.marketgravity.cameras.weather.repository;

import com.marketgravity.cameras.weather.domain.WeatherReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface WeatherReportRepository extends JpaRepository<WeatherReport, Integer> {

    List<WeatherReport> findByDtBetweenAndLocationId(long start, long end, int locationId);

}

