package com.marketgravity.cameras.weather.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.*;


public class DatesUtil {

     private static final  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("" +
            "[yyyy-MM-dd HH:mm:ss.S]" +
            "[yyyy-MM-dd'T'HH:mm:ss.SSS'Z']" +
            "[yyyy-MM-dd HH:mm:ss]" +
            "[yyyy-MM-dd'T'HH:mm:ss.SSS]" +
            "[yyyy-MM-dd'T'HH:mm]");

    public static LocalDateTime dateFormatter(String dateString) {
        return LocalDateTime.parse(dateString, formatter);
    }


    public static String dateToString(Date date, String timeZone) {

        String pattern = "yyyy-MM-dd HH:mm:ss.S";
        DateFormat df = new SimpleDateFormat(pattern);
        df.setTimeZone(TimeZone.getTimeZone(timeZone));
        return df.format(date);

    }

    public static String convertUnixDateToStrDate(long unixDate){
        // convert seconds to milliseconds
        Date date = new java.util.Date(unixDate*1000L);
        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(java.util.TimeZone.getTimeZone("Europe/London"));
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    public static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert, String zone) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.of(zone))
                .toLocalDateTime();
    }

    public static LocalDate convertToLocalDateViaInstant(Date dateToConvert,String zone) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.of(zone))
                .toLocalDate();
    }

    public static long timeConversion(String time) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS", Locale.ENGLISH); //Specify your locale

        long unixTime = 0;
        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/London")); //Specify your timezone
        try {
            unixTime = dateFormat.parse(time).getTime();
            unixTime = unixTime / 1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return unixTime;
    }
}
